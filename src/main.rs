mod config;

fn main() {
    let config = match config::get_configuration() {
        Ok(config) => Some(config),
        Err(_) => {
            println!("No config file");
            return;
        }
    };
    // connect to mastodon
    //   if fails, try to request a token
    //     if fails, try to register the app
    // connect to evernote
    // for every record on user favorites:
    //   if record id is the last one seen:
    //     stop
    //   create an evernote note
    //   save evernote note
    // save last seen favorite id to configuration file
    println!("{:?}", config);
}
