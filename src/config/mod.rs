use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

/// Configuration information
#[derive(Debug)]
pub struct Config {
    max_id: Option<u64>,
    user_id: String,
    server: String,
    token: String
}

/// Parse the configuration file
pub fn get_configuration() -> Result<Config, String> {
    let file = match File::open("m2e.toml") {
        Ok(file) => file,
        Err(_) => {
            return Err("Configuration file not found".to_string());
        }
    };

    let reader = BufReader::new(&file);

    for l in reader.lines() {
        let line = l.unwrap();
        println!("Line: {:?}", line);
        let mut parts = line.splitn(2, '=');
        let variable = parts.nth(0).unwrap().trim();
        let values:Vec<&str> = parts.skip(1).collect();
        let value = values.join("=");

        println!("Found variable {:?}, with value {:?}", variable, value);
    }

    Ok(Config { max_id: None, 
                user_id: "".to_string(),
                server: "".to_string(),
                token: "".to_string() })
}
